## Description
- Secure web messenger
- Use of RSA-2048 for key exchange
- Use of AES 256 bits key for message exchange

## Project structure
```
├── client
│   ├── app
│   │   ├── account
│   │   │   ├── login
│   │   │   ├── settings
│   │   │   └── signup
│   │   ├── admin
│   │   ├── chat
│   │   └── search
│   ├── assets
│   │   └── images
│   ├── components
│   │   ├── auth
│   │   ├── footer
│   │   ├── messages
│   │   ├── modal
│   │   ├── mongoose-error
│   │   ├── navbar
│   │   ├── socket
│   │   └── tab
│   └── forge
├── e2e
│   └── main
└── server
    ├── api
    │   ├── message
    │   └── user
    ├── auth
    │   └── local
    ├── components
    │   └── errors
    ├── config
    │   └── environment
    ├── serverkeys
    └── views
```
## Requirements
- Node (http://nodejs.org/)
- MongoDB (https://www.mongodb.org/)

## Installation
Install the dependencies by running the commands
```sh
$ npm install
$ bower install
```
## Usage
To start the server, execute the following command
```sh
$ grunt serve
```

## Deployment
So as to deploy the project, run the command
```sh
$ grunt build
```
Then host the ***dist*** directory

## License
The MIT License (MIT)

Copyright (c) 2015 Hassan El Omari Alaoui, Tobias Gottwald and Nguyen Ngoc Cao, KAIST, South Korea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.