/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

// Connect to database
console.log("mongo", config.mongo)
console.log("uri", config.mongo.uri)
console.log("options", config.mongo.options)
mongoose.connect(config.mongo.uri, config.mongo.options);
var db = mongoose.connection;
db.on('error', function (err) {
  console.log('Err:', err);
  throw err;
})
// Populate DB with sample data
if(config.seedDB) { require('./config/seed'); }

// Setup server
var fs = require('fs');
var privateKey  = fs.readFileSync('./server/serverkeys/key.pem', 'utf8');
var certificate = fs.readFileSync('./server/serverkeys/cert.pem', 'utf8');
var credentials = {key: privateKey, cert: certificate};
var app = express();
// var server = require('http').createServer(app);
var server = require('https').createServer(credentials, app);
var socketio = require('socket.io')(server, {
  serveClient: (config.env === 'production') ? false : true,
  path: '/socket.io-client'
});
require('./config/socketio')(socketio);
require('./config/express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;