/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {
  app.get('*',function(req,res,next){
    if(req.headers['x-forwarded-proto']=='http')
      res.redirect('https://' + req.headers.host + req.path)
    else
      next() /* Continue to other routes if we're not redirecting */
  });
  // Insert routes below
  app.use('/api/message', require('./api/message'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
