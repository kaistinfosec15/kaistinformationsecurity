'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var User = require('../../api/user/user.controller');

var router = express.Router();

router.post('/', function(req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    var error = err || info;
    if (error) return res.json(401, error);
    if (!user) return res.json(404, {message: 'Something went wrong, please try again.'});
    if (user.online) return res.json(500, {message: 'User is already online from another browser.'});

    var token = auth.signToken(user._id, user.role);
    User.online(user._id, function(err, user) {
      if (err) return res.send(500, err);
      res.json({token: token});
    })
  })(req, res, next)
});

module.exports = router;