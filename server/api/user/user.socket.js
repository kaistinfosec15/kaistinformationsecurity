'use strict';

var User = require('./user.model');
var Message = require('../message/message.model');

var socketUsers = [];

exports.register = function(socketio, socket) {

  // user logged for the first time
  socket.on('fstLogin', function(userId) {
    console.log('fstLogin', userId, socket.id);
    socketUsers[userId] = socket.id;
    emitToFriendsMe(socketio, userId, 'login');
  });

  // user logged for the first time or refreshed the page
  socket.on('login', function(userId) {
    console.log('login', userId, socket.id);
    socketUsers[userId] = socket.id;
  });
  

  // user logged out
  socket.on('logout', function(userId) {
    console.log('logout', userId, socket.id);
    emitToFriendsMe(socketio, userId, 'logout');
    delete socketUsers[userId];
  });

  // new friend request from the search site
  socket.on('friendRequest', function(user) {
    if (socketUsers.hasOwnProperty(user._id)) {
      socketio.to(socketUsers[user._id]).emit('friendRequest');
    }
  });

  // user added recommended friend, send that friend a notification if online
  socket.on('addRecommended', function(addRecommend) {
    if (socketUsers.hasOwnProperty(addRecommend.to))
      emit(socketio, addRecommend.to, 'addedFriend', {from: addRecommend.userId});
  });
  
  // session init
  socket.on('sessionInit', function(init){
    socketio.to(socketUsers[init.to]).emit('sessionInit', init);
  });
  
  // session init response (containing shared key)
  socket.on('sessionInitResponse', function(initresp){
    socketio.to(socketUsers[initresp.to]).emit('sessionInitResponse', initresp);
  });

  // message dropped
  socket.on('messageDropped', function (data) {
    var from = data.from;
    var to = data.to;
    socketio.to(socketUsers[to]).emit('messageDropped', from);
  });

  // new message
  socket.on('message', function(data) {
    var message = data.message;
    var from = search(socket.id);
    var to = data.to;
    var object = new Message({
      content: message,
      iv: data.iv,
      tag: data.tag,
      from: from,
      to: to
    });
    socketio.to(socketUsers[to]).emit('message', object);
    socket.emit('message', object); // Re-emits the message to the user
    // messages are not saved in the database
    // object.save(function(err, message) {
    //   if (err) return; // TODO design: display error message in UI
    // socketio.to(socketUsers[to]).emit('message', object);
    // socket.emit('message', object); // Re-emits the message to the user
    // });
  });
}

function search(socketid) {
  for (var userId in socketUsers) {
    if (socketUsers[userId] == socketid) return userId
  }
}

function emit(socketio, to, event, params, cb) {
  socketio.to(socketUsers[to]).emit(event, params, cb);
}

function emitToFriendsMe(socketio, userId, event) {
  User.findById(userId, '-salt -hashedPassword', function(err, user) {
    if (err) return;
    User.find({
      friends: {
        $in: [userId]
      }
    }, '-salt -hashedPassword', function(err, users) {
      if (err) return;
      users.forEach(function(u) {
        emit(socketio, u._id, event, user);
      });
    });
  });
}