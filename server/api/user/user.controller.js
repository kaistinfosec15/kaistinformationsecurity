'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');

var validationError = function(res, err) {
  return res.json(422, err);
};
console.log("user", new User({
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }))
/**
 * Get list of users
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.send(500, err);
    res.json(200, users);
  });
};

/**
 * Get list of users apart me
 */
exports.search = function(req, res) {
  var user = req.user;
  var filter = user.friends;
  filter.push(user.id);
  User.find({ _id: { $nin: filter } }, '-salt -hashedPassword', function (err, users) {
    if (err) return res.send(500, err);
    res.json(200, users);
  });
}

/**
 * Get list of mutual friends
 */
exports.friends = function(req, res) {
  var friends = req.user.friends;
  var options = req.body;
  options._id = { $in: friends };
  User.find(options, '-salt -hashedPassword')
    .where({ friends: req.user.id })
    .exec(function (err, users) {
    if (err) res.send(500, err);
    res.json(200, users);
  });
}

/**
 * Get list of recommended friends
 */
exports.recommended = function(req, res) {
  var user = req.user;
  var options = req.body || {};
  options.friends = { $in: [user._id] };
  options._id = { $nin: user.friends };
  User.find(options, '-salt -hashedPassword', function (err, users) {
    if (err) res.send(500, err);
    res.json(200, users);
  });
}

/**
 * Update user
 */
exports.update = function(userId, options, cb) {
  User.findByIdAndUpdate(userId, options, function (err, user) {
    if (err) cb(err);
    cb(null, user);
  });
}

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    exports.online(user._id, function(err, user) {
      if (err) return res.send(500, err)
      res.json({ token: token });
    })
  });
};

/**
 * Change online to true
 * @param  {String} userId user's id
 * @param  {Function} cb cb to call after completion
 */
exports.online = function(userId, cb) {
  exports.update(userId, { online: true }, cb);
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.send(500, err);
    return res.send(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.json(401);
    res.json(user);
  });
};

/**
 * Add friend
 */
exports.addFriend = function(req, res, next) {
  var userId = req.user._id;
  var friendId = req.params.id;
  var options = { $addToSet: { friends: friendId } };
  exports.update(userId, options, function(err, user) {
    if (err) return res.send(500, err);
    res.send(200);
  });
}

/**
 * Remove friend
 */
exports.removeFriend = function(req, res, next) {
  var userId = req.user._id;
  var friendId = req.params.id;
  var options = { $pull: { friends: friendId } };
  exports.update(userId, options, function(err, user) {
    if (err) return res.send(500, err);
    res.send(200);
  });
}

/**
 * User logout
 */
exports.logout = function(req, res, next) {
  var userId = req.params.id;
  exports.update(userId, { online: false }, function(err, user) {
    if (err) return res.send(500, err);
    res.send(200);
  })
}

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};
