'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/search', auth.isAuthenticated(), controller.search);
router.post('/friends', auth.isAuthenticated(), controller.friends);
router.post('/recommended', auth.isAuthenticated(), controller.recommended);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.post('/logout/:id', controller.logout);
router.post('/friend/:id', auth.isAuthenticated(), controller.addFriend);
router.delete('/friend/:id', auth.isAuthenticated(), controller.removeFriend);

module.exports = router;
