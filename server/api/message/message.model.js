'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MessageSchema = new Schema({
  content: String,
  iv: String,
  tag: String,
  from: Schema.Types.ObjectId, 
  to: Schema.Types.ObjectId
});

module.exports = mongoose.model('Message', MessageSchema);
