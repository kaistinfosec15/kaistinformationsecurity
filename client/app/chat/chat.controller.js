'use strict';

angular.module('secureChatApp')
  .controller('ChatCtrl', function($scope, $http, socket, User, Key, Session, Messages) {
  
    $scope.finishedKeygen = false;
    // Listen for the keygen finish event
    Key.listen(function(){
      $scope.finishedKeygen = true;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    });
    
    $scope.friend = null;
    
    // friends
    User.friends({
      online: true
    }, function(friends) {
      $scope.friends = friends;
    });
    // friends that the user clicked on
    $scope.tabs = [];
    // Tracker how many messages are unseen in a tab
    $scope.unseen = [];
    // me
    User.get().$promise
      .then(function(me) {
        $scope.me = me;
        Messages.init(me);
        $scope.messages = Messages.getMessages();
        socket.socket.emit('login', me._id);
      });

    /**
     * Add a tab when the user clicks on a friend
     * @param {Object} tab friend to add
     */
    $scope.addTab = function(tab) {
      $scope._addTab(tab)
      $scope.active(tab);
    };

    /**
     * Add tab without activating it
     * @param {Object} tab friend to add
     */
    $scope._addTab = function(tab) {
      if ($.inArray(tab, $scope.tabs) == -1) {
        $scope.tabs.unshift(tab);
        $scope.unseen.unshift(0);
      }
    };

    /**
     * Remove tab when the user clicks on remove button
     * @param  {Object} tab tab to remove
     */
    $scope.removeTab = function(tab) {
      var index = $scope.tabs.indexOf(tab);
      if (index != -1) {
        $scope.tabs.splice(index, 1);
        $scope.unseen.splice(index, 1);
      }
      $scope.active($scope.tabs[0]);
    };
    
    /**
     * Remove tab by id
     * @param  {Number} tab tab to remove
     */
    $scope.removeTabById = function(tabId) {
      for (var i=0; i< $scope.tabs.length; ++i){
        if ($scope.tabs[i]._id == tabId){
          if ($scope.tabs[i].active) $scope.active($scope.tabs[0]);
          $scope.tabs.splice(i, 1);
          $scope.unseen.splice(i, 1);
        }
      }
    };

    /**
     * Active a tab
     * @param  {Object} tab tab to active
     */
    $scope.active = function(tab) {
      // Update preshared values for old tab if there is one
      if ($scope.friend)
        $scope.synchPreSharedToSession(Session.getSession($scope.friend._id));
      // deactive all the other tabs
      angular.forEach($scope.tabs, function(t) {
        t.active = false;
      });
      $scope.friend = tab;
      if (tab != undefined) {
        var index = $scope.tabs.indexOf(tab);
        $scope.unseen[index] = 0;
        // active tab
        tab.active = true;
        $scope.resetPreSharedFields();
        $scope.synchPreSharedFromSession(Session.getSession(tab._id));
      }
    };

    /**
     * Check whether a tab is active or not
     * Return true if it's active, false otherwise
     * @param  {Object}  tab tab to check
     * @return {Boolean}
     */
    $scope.isActive = function(tab) {
      return tab.active;
    };

    /**
     * Remove friend from database
     * @param  {Object} user user to remove
     */
    $scope.removeFriend = function(user, $event) {
      // stopPropagation so as to avoid adding tab
      $event.stopPropagation();
      // preventDefault so as to avoid refreshing the page
      $event.preventDefault();

      User.removeFriend({
          controller: user._id
        }).$promise
        .then(function() {
          console.log('success');
          $scope._removeFriend(user);
          // call User.recommended in NavbarRecommendedCtrl
          $scope.$broadcast('refresh');
        })
        .catch(function() {
          console.log('error');
        });
    };

    /**
     * Add friend to array friends
     * @param  {Object} user user to add
     */
    $scope._addFriend = function(user) {
      if ($scope.existFriend(user) == -1) $scope.friends.push(user);
    };

    /**
     * Remove friend from array friends
     * @param  {Object} user user to remove
     */
    $scope._removeFriend = function(user) {
      var index;
      if ((index = $scope.existFriend(user)) != -1) $scope.friends.splice(index, 1);
      // Remove the session attached to the friend if it exists
      Session.removeSession(user._id);
      // Remove tab
      $scope.removeTab(user);
    };

    /**
     * Remove friend by id from array friends
     * @param  {Object} user user to remove
     */
    $scope._removeFriendById = function(userId) {
      angular.forEach($scope.friends, function(user, index) {
        if (user._id === userId) $scope.friends.splice(index, 1);
      });
      // Remove the session attached to the friend if it exists
      Session.removeSession(userId);
      // Remove the tab
      $scope.removeTabById(userId);
    };

    /**
     * Check if a friend exist
     * Return index of user if exist, -1 otherwise
     * @param  {Object} user user to check
     * @return {int} index of user
     */
    $scope.existFriend = function(user) {
      return $scope.friends.indexOf(user)
    };

    /**
     * A friend login
     * @param  {String} userId event name
     */
    socket.socket.on('login', function(user) {
      console.log('login');
      $scope._addFriend(user);
    });

    /**
     * A friend logged out
     * @param  {String} userId event name
     */
    socket.socket.on('logout', function(user) {
      console.log('logout');
      $scope._removeFriendById(user._id);
    });
    
    /**
     * A friend added us from recommended friends
     * @param  {String} userId event name
     */
    socket.socket.on('addedFriend', function (user) {
      // Refresh friends
      User.friends({
        online: true
      }, function (friends) {
          $scope.friends = friends;
        });
    });
    
    /**
     * Checks and updates the current pre-shared key fields from the session, used on tab activate
     * @param {Object} session
     */
    $scope.synchPreSharedFromSession = function(session) {
      if (session == null) {
        return;
      }
      if ($scope.friend != null && session.toId == $scope.friend._id) {
        if ($scope.scopeNewMessage != null && session.usePreShared == true) {
          $scope.scopeNewMessage.usePreShared = true;
          $scope.scopeNewMessage.preSharedKey = session.preSharedKey;
        }
        else if ($scope.scopeNewMessage != null) {
          $scope.scopeNewMessage.usePreShared = false;
          $scope.scopeNewMessage.preSharedKey = '';
        }
      }
    };
    
    /**
     * Synchs the pre shared values to the session used on tab deactivate, message receive and message send
     * @param {Object} session
     */
    $scope.synchPreSharedToSession = function(session) {
      if (session == null) {
        return;
      }
      if ($scope.friend != null && session.toId == $scope.friend._id) {
        if ($scope.scopeNewMessage != null && $scope.scopeNewMessage.usePreShared == true) {
          session.usePreShared = true;
          session.preSharedKey = $scope.scopeNewMessage.preSharedKey;
        }
        else {
          session.usePreShared = false;
          session.preSharedKey = '';
        }
      }
    };
    
    /**
     * Reset the pre shared key html fields
     * @param 
     */
    $scope.resetPreSharedFields = function() {
      if ($scope.scopeNewMessage) {
        $scope.scopeNewMessage.usePreShared = false;
        $scope.scopeNewMessage.preSharedKey = '';
      }
    };
    
    /**
     * Helper for message encryption
     * @param  {Object} cipheredMessage session
     */
    $scope.encryptMessage = function(message, session) {
      var iv = forge.random.getBytesSync(12);
      var cipher = null;
      if (session.usePreShared) {
        var md = forge.md.sha256.create();
        md.update(session.preSharedKey);
        cipher = forge.cipher.createCipher('AES-GCM', md.digest());      
      }
      else {
        cipher = forge.cipher.createCipher('AES-GCM', session.sharedKey);
      }
        
      cipher.start({iv: iv});
      cipher.update(forge.util.createBuffer(message));
      cipher.finish();
      var encrypted = cipher.output.getBytes();
      var tag = cipher.mode.tag.getBytes();
      var cipheredMessage = {
            message: encrypted,
            iv: iv,
            tag: tag,
            to: session.toId
          };
      return cipheredMessage;
    };
    
    /**
     * Helper for message decryption
     * @param  {Object} cipheredMessage session
     */
    $scope.decryptMessage = function(cipheredMessage, session) {
      var decipher = null;
      if (session.usePreShared) {
        var md = forge.md.sha256.create();
        md.update(session.preSharedKey);
        decipher = forge.cipher.createDecipher('AES-GCM', md.digest());
      }
      else {
        decipher = forge.cipher.createDecipher('AES-GCM', session.sharedKey);
      }
      decipher.start({iv: cipheredMessage.iv, tag: cipheredMessage.tag});
      decipher.update(forge.util.createBuffer(cipheredMessage.content));
      var pass = decipher.finish();
      if (pass) {
        return decipher.output.data;
      }
      else {
        console.log('Error decrypting message ' + cipheredMessage.content);
        return null;
      }
    };
    
    /**
     * Init a new session
     * @param {String} friendsid
     */
    $scope.sessionInit = function(friendId) {
      // Create new session obj
      var session = Session.addSession(friendId);
      var pk = { n: Key.publicKey().n.toString(16), e: Key.publicKey().e.toString(16) };
      // Init a new session
      socket.socket.emit('sessionInit', {
        from: $scope.me._id,
        to: session.toId,
        pubKey: pk
      });
      return session;
    };
    
    /**
     * Receive a session init, containing the initiating user id and his pubkey
     * @param  {Object} init
     */
    socket.socket.on('sessionInit', function(init) {
      // Generate a key encapsulation
      var kdf1 = new forge.kem.kdf1(forge.md.sha1.create());
      var kem = forge.kem.rsa.create(kdf1);
      var n = new forge.jsbn.BigInteger(init.pubKey.n, 16);
      var e = new forge.jsbn.BigInteger(init.pubKey.e, 16);
      init.pubKey = forge.pki.rsa.setPublicKey(n, e);

      var result = kem.encrypt(init.pubKey, 16); // Containing encapsulated key and key

      // Generate our session side, automagically overrides
      var session = Session.addSession(init.from);
      session.sharedKey = result.key;
      session.active = true;
      
      console.log(forge.util.encode64(result.key));
      var encodedEncaps = forge.util.encode64(result.encapsulation);
      // Push out the session init response
      socket.socket.emit('sessionInitResponse', {
        from: $scope.me._id,
        to: session.toId,
        sharedKey: encodedEncaps
      });
    });
    
    /**
     * Receive a session init response, containing the responding id and the chosen shared key
     * @param  {Object} initResp
     */
    socket.socket.on('sessionInitResponse', function(initResp) {
      var session = Session.getSession(initResp.from);
      // If we found a session, update it
      if (session != null) {
        initResp.sharedKey = forge.util.decode64(initResp.sharedKey);
        session.active = true;
        // Decrypt the encapsulated shared key
        var kdf1 = new forge.kem.kdf1(forge.md.sha1.create());
        var kem = forge.kem.rsa.create(kdf1);
        session.sharedKey = kem.decrypt(Key.privateKey(), initResp.sharedKey, 16);
        console.log(forge.util.encode64(session.sharedKey));
        
        // Push out messages
        for (var i=0; i < session.tmpMsg.length; ++i) {
          var ciphered = $scope.encryptMessage(session.tmpMsg[i], session);
          socket.socket.emit('message', ciphered);
        }
        // Delete useless messages
        delete session.tmpMsg;
      }
      else { // If theres no session, we have to restart the initiation process
        $scope.sessionInit(initResp.from);
        Messages.addMessage({from: friend._id, withId:friend._id, status:'warning', content:'Session response without associated session arrived. Session is reinitialized!'})
      }
    });
    
    
    /**
     * Receive message
     * @param  {Object} message
     */
    socket.socket.on('message', function(message) {
      var session = null;
      if (message.from != $scope.me._id) {
        var friend = $scope.search(message.from);
        session = Session.getSession(message.from);
        if (friend != null) $scope._addTab(friend);
        // Add message tracker
        if ($scope.friend != friend){
          var index = $scope.tabs.indexOf(friend);
          $scope.unseen[index] += 1;
        }
        
        // If no session, Discard message and try to reestablish session. 
        // Also write a warning about it
        if (session == null) {
          $scope.sessionInit(message.from);
          Messages.addMessage({from: friend._id, withId:friend._id, status:'warning', content:'Message without associated session arrived and has been dropped. Session is reinitialized!'});
          socket.socket.emit('messageDropped', {from: $scope.me, to: friend._id});
          return;
        }
      }
      else {
        session = Session.getSession(message.to); // message came from ourselves
        // If no session, Discard message and try to reestablish session. 
        // Also write a warning about it
        if (session == null) {
          $scope.sessionInit(message.to);
          Messages.addMessage({from: friend._id, withId:friend._id, status:'warning', content:'Message without associated session arrived and has been dropped. Session is reinitialized!'});
          return;
        }
      }
      $scope.synchPreSharedToSession(session);
      // Decrypt
      var decrypted = $scope.decryptMessage(message, session);
      message.ciphertext = message.content
      if (decrypted != null) {
        message.content = decrypted;
        message.withId = session.toId;
        Messages.addMessage(message);
      }
    });

    /**
     * Message dropped
     */
    socket.socket.on('messageDropped', function (from) {
      console.log('message dropped', from)
      Messages.addMessage({from: from._id, withId:from._id, status:'warning', content:'The last message has been dropped. Session is reinitialized!'});
    })

    /**
     * Search friend by id and return it
     * @param  {String} userId id of the user to search
     * @return {Object} friend
     */
    $scope.search = function(userId) {
      var friend = null;
      $scope.friends.every(function(f) {
        if (f._id == userId) {
          friend = f;
          return false;
        }
        return true;
      });
      return friend;
    };

    $scope.$on('scope', function (e, scope) {
      e.preventDefault();
      e.stopPropagation();
      $scope.scopeNewMessage = scope;
    });
    
    /**
     * Send message
     * @param  {event} e
     */
    $scope.sendMessage = function(e) {
      // Check for enter key
      var shiftKey = e.shiftKey;
      var key = e.keyCode || e.which;
      if (key == 13 && !shiftKey) { // ENTER
          e.preventDefault();
          $scope.sendMessageBtn();
      }
    };
    
    $scope.sendMessageBtn = function() {
      var message = $scope.scopeNewMessage.newMessage;
      if (typeof message != "undefined" && message != "") {
        // Check if we currently have a session initiated to the person we
        // want to send our message to
        var session = Session.getSession($scope.friend._id);
        if (session == null) {
          session = $scope.sessionInit($scope.friend._id);
        }
		  	$scope.synchPreSharedToSession(session);
        // Check if the session is already active (this always fails with newly created ones)
        if (session.active) {
          // send message
          var ciphered = $scope.encryptMessage(message, session);
          socket.socket.emit('message', ciphered);
        }
        else { // Not active yet, store the message temporarily and send once active
          session.tmpMsg.push(message);
        }
        $scope.scopeNewMessage.newMessage = "";
      }
    };

    /**
     * Resolves a userid to a name
     * @param  {String} userId
     */
    $scope.resolveName = function(userId){
      if (userId == $scope.me._id)
        return $scope.me.name;
      return $scope.friends.filter(function( obj ) {
        return obj._id == userId;
      })[0].name;
    };

    $scope.$on('$destroy', function() {
      // Clean up socket listeners
      socket.removeListener('message');
      socket.removeListener('sessionInitResponse');
      socket.removeListener('sessionInit');
      socket.removeListener('addedFriend');
      socket.removeListener('login');
      socket.removeListener('logout');
    });
  });