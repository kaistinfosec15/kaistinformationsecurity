'use strict';

angular.module('secureChatApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/chat', {
        templateUrl: 'app/chat/chat.html',
        controller: 'ChatCtrl'
      })
      .when('/', {
        redirectTo: '/chat'
      });
  });
