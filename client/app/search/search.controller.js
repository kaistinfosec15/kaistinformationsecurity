'use strict';

angular.module('secureChatApp')
  .controller('SearchCtrl', function ($scope, $http, socket, User) {

    // all users except 'me' and friends
    $scope.users = User.search(function() {

    }, function(err) {
        console.log('Error', err);
    });
    // me
    User.get().$promise
      .then(function(me) {
        $scope.me = me;
        socket.socket.emit('login', me._id);
      });

    $scope.add = function(user) {
      if (!user) return;
      User.addFriend({controller: user._id}).$promise
        .then(function() {
          // send signal via socket.io to the other user
          // so that he refreshes his recommended friends
          // normally the emit function emit the message only to the user concerned
          socket.socket.emit('friendRequest', user);

          console.log('success');
          // TODO design: show message to tell the user that he added a friend
          // TODO design: animate with css while the div of user is removed
          var index = $scope.users.indexOf(user);
          if (index != -1) {
            $scope.users.splice(index, 1);
          }
        })
        .catch(function() {
          // TODO design: show message to tell the user that an error occured
          console.log('error');
        });
    }

  });
