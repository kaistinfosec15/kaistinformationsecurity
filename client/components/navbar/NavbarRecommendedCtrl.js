'use strict';

angular.module('secureChatApp')
  .controller('NavbarRecommendedCtrl', function ($scope, $http, socket, Auth, User) {

    // recommended friend
    $scope.recommended = User.recommended();

    /**
     * Add recommended user as friend
     * @param {Object} user user to add
     */
    $scope.add = function(user) {
      if ($.inArray(user, $scope.recommended) == -1) return;
      User.addFriend({controller: user._id}).$promise
        .then(function() {
          console.log('success');
          var index = $scope.recommended.indexOf(user);
          if (index != -1) {
            $scope.recommended.splice(index, 1);
            // Make online check and then add to friendslist
            $http.get('/api/users/' + user._id).success(function(){
              $scope.$parent._addFriend(user);
            });
          }
        })
        .catch(function() {
          console.log('error');
        });
      // Also send a socket event to the other user
      socket.socket.emit('addRecommended', {userId: Auth.getCurrentUser(), to: user._id});
    };

    $scope.refresh = function() {
      $scope.recommended = User.recommended();
    };

    /**
     * A user added me as a friend
     * @param  {String} name of the event
     * @param  {function} callback 
     * @return {[type]}   [description]
     */
    socket.socket.on('friendRequest', function() {
      $scope.refresh();
    });

    /**
     * Refresh Ctrl
     * @param  {String} name of the event
     */
    $scope.$on('refresh', function() {
      $scope.refresh();
    });
    
    $scope.$on('$destroy', function() {
      // Clean up socket listeners
      socket.removeListener('friendRequest');
    });

  });
