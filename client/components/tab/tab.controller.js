'use strict';

angular.module('secureChatApp')
  .controller('TabCtrl', function($scope, $http, socket, User, Key, Session, Messages) {

    $scope.$emit('scope', $scope);
    $scope.sendMessage = function($event) {
      $scope.$parent.sendMessage($event);
    }
  });