'use strict';

angular.module('secureChatApp')
  .factory('Messages', function(localStorageService) {

    // Messages    
    var messages;
    var me = null;

    return {
      init: function(user) {
        me = user._id;
        messages = localStorageService.get('messages[' + me + ']') || {};
      },
      getMessages: function() {
        return messages;
      },
      addMessage: function(message) {
        messages[message.withId] = messages[message.withId] ||  []
        messages[message.withId].push(message);
        localStorageService.set('messages[' + me + ']', messages);
      }
    }
  });