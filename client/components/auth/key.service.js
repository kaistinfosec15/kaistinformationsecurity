'use strict';

angular.module('secureChatApp')
  .factory('Key', function(localStorageService){
    var rsa = forge.pki.rsa;

    // Listener for getting notified once we finished generating a key
    var keypair = null;
    var cbs = [];
    function listen(cb) {
      if (keypair) {
        return cb();
      }
      cbs.push(cb);
    }

    function StoreKey() {
      var pubKey = keypair.publicKey;
      var n = pubKey.n.toString(16);
      var e = pubKey.e.toString(16);
      var privKey = keypair.privateKey;
      var d = privKey.d.toString(16);
      var p = privKey.p.toString(16);
      var q = privKey.q.toString(16);
      var dP = privKey.dP.toString(16);
      var dQ = privKey.dQ.toString(16);
      var qInv = privKey.qInv.toString(16);
      localStorageService.set('rsapair', {publicKey: {n:n, e:e}, 
        privateKey: {d:d, p:p, q:q, dP:dP, dQ:dQ, qInv:qInv}});
    }
    
    function RetrieveKey() {
      var keyobj = localStorageService.get('rsapair');
      var n = new forge.jsbn.BigInteger(keyobj.publicKey.n, 16);
      var e = new forge.jsbn.BigInteger(keyobj.publicKey.e, 16);
      var d = new forge.jsbn.BigInteger(keyobj.privateKey.d, 16);
      var p = new forge.jsbn.BigInteger(keyobj.privateKey.p, 16);
      var q = new forge.jsbn.BigInteger(keyobj.privateKey.q, 16);
      var dP = new forge.jsbn.BigInteger(keyobj.privateKey.dP, 16);
      var dQ = new forge.jsbn.BigInteger(keyobj.privateKey.dQ, 16);
      var qInv = new forge.jsbn.BigInteger(keyobj.privateKey.qInv, 16);
      keypair = {
        privateKey: rsa.setPrivateKey(n, e, d, p, q, dP, dQ, qInv), 
        publicKey: rsa.setPublicKey(n, e)};
    }
    
    // generate an RSA key pair asynchronously
    if (localStorageService.get('rsapair')) {
      RetrieveKey();
    }
    else {
      rsa.generateKeyPair({bits: 2048, workers: -1}, function(err, keys){
        keypair = keys;
        StoreKey();
        
        for (var i=0; i< cbs.length; ++i) {
          cbs[i]();
        }
      });
    }
    
    function getPub() {
      if (keypair) {
        return keypair.publicKey;
      }
      return null;
    }
    
    function getPriv() {
      if (keypair) {
        return keypair.privateKey;
      }
      return null;
    }
    
    return {listen: listen, publicKey: getPub, privateKey: getPriv};
  });