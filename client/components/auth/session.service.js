'use strict';

angular.module('secureChatApp')
  .factory('Session', function() {
    
    // Sessions
    // All friends we initiated a communication with and the exchanged key
    // Sessions are objects in the following format:
    // {toId: String, sharedKey: forge key object, active: Bool, tmpMsg: [String]}
    // Tmp messages will be stored before the session is fully active,
    // Once the session is fully established, they will be pushed out and removed from object
    var sessions = {};
    
    return {
      addSession: function(toId) {
        sessions[toId] = {toId: toId, sharedKey: null, active: false, 
          tmpMsg: [], usePreShared: false, preSharedKey: ''};
        return sessions[toId];
      },
      removeSession: function(toId) {
        if (sessions.hasOwnProperty(toId)) {
          delete sessions[toId];
        }
      },
      getSession: function(toId) {
        if (sessions.hasOwnProperty(toId)) {
          return sessions[toId];
        }
        else {
          return null;
        }
      }
    };
  });