'use strict';

angular.module('secureChatApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id/:controller', {
      id: '@_id', controller: '@controller'
    },
    {
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      },
      search: {
        method: 'GET',
        params: {
          id: 'search'
        },
        isArray: true
      },
      addFriend: {
        method: 'POST',
        params: {
          id: 'friend'
        }
      },
      removeFriend: {
        method: 'DELETE',
        params: {
          id: 'friend'
        }
      },
      friends: {
        method: 'POST',
        params: {
          id: 'friends'
        },
        isArray: true
      },
      recommended: {
        method: 'POST',
        params: {
          id: 'recommended'
        },
        isArray: true
      },
      logout: {
        method: 'POST',
        params: {
          id: 'logout'
        }
      }
	  });
  });
